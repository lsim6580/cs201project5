/*********************************************
This project focused on using pointers and dynamic arrays


Name: 	Luke Simmons

Course:
ECE 216
Program: 	Program 5
Due Date: 	3/31/2015
Description: We have a conference going on. We need to write a program that will that will keep track of the Attendees and
the number of students that each one will take to the conference.
Inputs: 	input will get the input file from the user. output will get the output from the user.
Outputs: 	we will output the file to the file that the user has specified.
Algorithm: 	
1   write a Class Attendee with the following private variables:
    Attendee Name, Institution name, ID number, Invoice number. two pointers one for a full conference array and one for a contest only array.
2.  the class should have the following functions:
  A  getters and setters for - Name,Institution,ID,Invoice.
  B  since we will deal with dynamic arrays we will need a destructor, a copy constructor , and an overloaded assignment operator.
  C  an Add and remove method for both arrays, This will be a boolean function and will either add or remove a name to the specified array.
  D  overloading the >> operator- wil read in the data from the input file this will set the all of the private variables as well as
        set the size for both arrays and fill up the arrays.
   E overloading the << operator- we will use this to display the information from out array.
   F A Balance method that will output the total cost that each Attendee will owe.
3.  The main function will need to create a dynami array full all the attendees(resize as needed)
4.  The array will need to be sorted by Name of the attendee.
5. output The Name,institition,ID,invoice, number of each kind of student, and total balance for each Attendee.
    at the bottom you should display the total number of attendees and the total number of each kind of student and the total cost.


