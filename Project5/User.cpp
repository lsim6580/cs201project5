#include "User.h"

User::User()
{
	ID = 0; 
	ArrSize = ArrUsed = 0; 
	CheckedOut = nullptr; 
}

User::~User()
{
	if (CheckedOut != nullptr)
		delete[] CheckedOut; 
}

const User& User::operator=(const User& rhs)
{
	if (this != &rhs) // 1. avoid A = A 
	{
		if (CheckedOut != nullptr) // 2. delete dynamic data if any 
		{
			delete[] CheckedOut; 
			CheckedOut = nullptr; 
			ArrSize = ArrUsed = 0; 
		}
		Name = rhs.Name;	// 3. copy static data 
		ID = rhs.ID; 
		ArrSize = rhs.ArrSize; 
		ArrUsed = rhs.ArrUsed; 
		if (ArrSize > 0)	// 4. copy dynamic data if any 
		{
			CheckedOut = new string[ArrSize]; 
			for (int k = 0; k < ArrUsed; k++)
				CheckedOut[k] = rhs.CheckedOut[k]; 
		}
	}
	return *this;  // 5. return 


}

User::User(const User& other)
{
	CheckedOut = nullptr; 
	*this = other; 
} 

void User::CheckOut(string item)
{
	if (ArrSize == ArrUsed) // we're full! 
	{
		string *TPtr = new string[ArrSize + 10]; 
		ArrSize += 10; 
		for (int k = 0; k < ArrUsed; k++)
			TPtr[k] = CheckedOut[k]; 
		delete[] CheckedOut; 
		CheckedOut = TPtr; 
	}
	CheckedOut[ArrUsed] = item; 
	ArrUsed++; 
}
#endif