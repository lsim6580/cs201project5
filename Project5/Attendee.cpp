#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "Attendee.h"
using namespace std;

Attendee::Attendee()// default constructor
{
    name = "";
    institution = "";
    ID = 0;
    contestarrused, contestsize, fullarrused, fullsize = 0;
    invoice = 0;
    fullAttendees = nullptr;
    contestAttendees = nullptr;
}
Attendee::~Attendee()//deconstructor
{
    if (!(fullAttendees == nullptr))
        delete[] fullAttendees;
    if (!(contestAttendees == nullptr))
        delete[] contestAttendees;
}
Attendee::Attendee(const Attendee& other)//copy constructor
{
    fullAttendees = nullptr;
    contestAttendees = nullptr;
    *this = other;
}
void Attendee::setName(string setname)// accepts a string and sets name to the parameter
{
    name = setname;
}
void Attendee::setinstitution(string inst)// accepts a string and sets the institution to the parameter
{
    institution = inst;
}
void Attendee::setID(int number)// accepts an int and sets the ID to the parameter
{
    if (number > 0)
    {
        ID = number;
    }
}
void Attendee::setinvoice(int number)// accepts an int and sets the invoice to the parameter
{
    if (number > 0)
    {
        invoice = number;
    }
}
string Attendee::getName()// returns the name
{
    return name;
}
string Attendee::getinstitution()// returns the institution
{
    return institution;
}
int Attendee::getID()// returns the ID
{
    return ID;
}
int Attendee::getinvoice()//returns the invoice number
{
    return invoice;
}
int Attendee::getFullsize()//will return the number of elements in the full conference list
{
    return fullarrused;
}
int Attendee::getcontestSize()//return the number of elements in the contest conference list
{
    return contestarrused;
}
const double Attendee::Balance()// will return the total cost of each attendee
{
    double balance = 150 + ((45 * fullarrused) + (20 * contestarrused));
    return balance;
}
const Attendee& Attendee::operator= (const Attendee& rhs)// this will allow us to set one instance of Attendee to another instance.
{
    if (this != &rhs)
    {
        if (!(fullAttendees == nullptr))
        {
            delete[] fullAttendees;// delete existing memory
            fullAttendees = nullptr;
            fullarrused = fullsize = 0;
        }
        if (!(contestAttendees == nullptr))// delete existing memory
        {
            delete[] contestAttendees;
            contestAttendees = nullptr;
            contestarrused = contestsize = 0;
        }
        // set lhs = rhs
        name = rhs.name;
        ID = rhs.ID;
        institution = rhs.institution;
        invoice = rhs.invoice;
        fullsize = rhs.fullsize;
        contestsize = rhs.contestsize;
        fullarrused = rhs.fullarrused;
        contestarrused = rhs.contestarrused;
        if (fullsize > 0)
        {
            fullAttendees = new string[fullsize];
            for (int k = 0; k < fullarrused; k++)
                fullAttendees[k] = rhs.fullAttendees[k];
        }
        if (contestsize > 0)
        {
            contestAttendees = new string[contestsize];
            for (int k = 0; k < contestsize; k++)
                contestAttendees[k] = rhs.contestAttendees[k];
        }

    }
    return *this;
}
bool Attendee::addfull(string name)// this will allow us to add another name to the list of students in the full list
{
    for (int k = 0; k < fullsize; k++)
    {
        if (name == fullAttendees[k])
            return false;
    }
    if (fullsize == fullarrused)
    {
        int biggersize = fullsize + 1;
        string *newfullarray = new string[biggersize];
        for (int k = 0; k < fullsize; k++)
        {
            newfullarray[k] = fullAttendees[k];
        }
        delete[] fullAttendees;
        fullsize = biggersize;
        fullAttendees = newfullarray;
    }
    fullAttendees[fullarrused] = name;
    fullarrused += 1;
    return true;
}
bool Attendee::removefull(string name)// this will allow us to remove a name from the list of students 
{
    bool isFound = false;
    int foundPosition;
    for (int k = 0; k < fullarrused; k++)
    {
        if (fullAttendees[k] == name)
        {
            isFound = true;
            foundPosition = k;
        }
    }
    if (isFound == false)
        return false;
    for (int k = foundPosition; k < fullarrused; k++)
    {
        if (k == fullarrused-1)
            fullAttendees[k] = "";
        else fullAttendees[k] = fullAttendees[k + 1];
    }
    fullarrused -= 1;
    return true;
}
bool Attendee::addcontest(string name)// this method will allow us to add a student to the list of students in just the conference list
{
    for (int k = 0; k < contestsize; k++)//try to find the name in the array
    {
        if (name == contestAttendees[k])
            return false;//return false if the name is already in the array
    }
    if (contestsize == contestarrused)// resize the array if needed
    {
        int biggersize = contestsize + 1;
        string *newcontestarray = new string[biggersize];
        for (int k = 0; k < contestsize; k++)
        {
            newcontestarray[k] = contestAttendees[k];
        }
        delete[] contestAttendees;
        contestsize = biggersize;
        contestAttendees = newcontestarray;
    }
    contestAttendees[contestarrused] = name;
    contestarrused += 1;
    return true;//return true if the name has been added to the array
}
bool Attendee::removecontest(string name)// this method will allow us to remove a name from the list
{
    bool isFound = false;
    int foundPosition;
    for (int k = 0; k < contestarrused; k++)// try to find the name to remove
    {
        if (contestAttendees[k] == name)
        {
            isFound = true;
            foundPosition = k;
        }
    }
    if (isFound == false)
        return false;// return false if the name is not in the array
    for (int k = foundPosition; k < contestarrused; k++)//shift all the items to the left of the array
    {
        if (k == contestarrused-1)
            contestAttendees[k] = "";
        else contestAttendees[k] = contestAttendees[k + 1];
    }
    contestarrused -= 1;
    return true;
}
ofstream& operator << (ofstream& out, Attendee& A)// this will overload the output operator 
{
    out << A.getName() << endl << A.getinstitution() << endl << A.getID() << " " << A.getinvoice() << endl;
    out << A.getFullsize() << endl;
    for (int k = 0; k < A.fullarrused; k++)
    {
        out << A.fullAttendees[k] << endl;
    }
    out << A.getcontestSize() << endl;
    for (int k = 0; k < A.contestarrused; k++)
    {
        out << A.contestAttendees[k] << endl;
    }
    return out;
}
ifstream& operator >> (ifstream& in, Attendee& A)// this will overload the input operator
{
    if (A.fullAttendees != nullptr)//delete our dynamic memory
        delete[] A.fullAttendees;
    if (A.contestAttendees != nullptr)//delete our dynamic memory
        delete[] A.contestAttendees;
    string streamline;
    stringstream line;
    getline(in, A.name);//set the name of the Attendee
    getline(in, A.institution);//set the name of the institution
    getline(in, streamline);
    line << streamline;
    line >> A.ID >> A.invoice;
    line.clear();
    getline(in, streamline);
    line << streamline;
    line >> A.fullsize;
    line.clear();
    string* newFull = new string[A.fullsize];// fill up our array
    for (A.fullarrused = 0; A.fullarrused < A.fullsize; A.fullarrused++)
    {
        getline(in, newFull[A.fullarrused]);
    }
    A.fullAttendees = newFull;
    getline(in, streamline);
    line << streamline;
    line >> A.contestsize;
    line.clear();
    string* newContest = new string[A.contestsize];//fill up our array
    for (A.contestarrused = 0; A.contestarrused < A.contestsize; A.contestarrused++)
    {
        getline(in, newContest[A.contestarrused]);
    }
    A.contestAttendees = newContest;
    return in;
}