

/*********************************************

Name: 	Luke Simmons

Course:
ECE 216
Program: 	Program 5
Due Date: 	3/31/2015
Description: We have a conference going on. We need to write a program that will that will keep track of the Attendees and
the number of students that each one will take to the conference.
Inputs: 	input will get the input file from the user. output will get the output from the user.
Outputs: 	we will output the file to the file that the user has specified.
Algorithm: 	
1   write a Class Attendee with the following private variables:
    Attendee Name, Institution name, ID number, Invoice number. two pointers one for a full conference array and one for a contest only array.
2.  the class should have the following functions:
  A  getters and setters for - Name,Institution,ID,Invoice.
  B  since we will deal with dynamic arrays we will need a destructor, a copy constructor , and an overloaded assignment operator.
  C  an Add and remove method for both arrays, This will be a boolean function and will either add or remove a name to the specified array.
  D  overloading the >> operator- wil read in the data from the input file this will set the all of the private variables as well as
        set the size for both arrays and fill up the arrays.
   E overloading the << operator- we will use this to display the information from out array.
   F A Balance method that will output the total cost that each Attendee will owe.
3.  The main function will need to create a dynami array full all the attendees(resize as needed)
4.  The array will need to be sorted by Name of the attendee.
5. output The Name,institition,ID,invoice, number of each kind of student, and total balance for each Attendee.
    at the bottom you should display the total number of attendees and the total number of each kind of student and the total cost.



**********************************************/

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include "Attendee.h"
int resizeArray(Attendee* &A, int oldsize)// accepts the pointer to an array and the size of the array. returns the new size of the array
{
    int newsize = oldsize + 1;
    Attendee* newlist = new Attendee[newsize];// make a new array
    // copy into new array
    for (int k = 0; k < oldsize; k++)
    {
        newlist[k] = A[k];
    }
    delete[] A;
    A = newlist;
    return newsize;
}
void sortArray(Attendee* &A, int size)// accepts the pointer to an array. will sort the array by the name of the attendee
{
    int n = 0;
    int newarray = 0;
    Attendee temp;
    int large = 0;
    for (int i = 0; i < size; i++)
    {
        for (int j = 0; j < size - 1; j++)
        {
            if (A[j].getName() > A[j + 1].getName())
            {
                temp = A[j];
                A[j] = A[j + 1];
                A[j + 1] = temp;
            }
        }
    }
}

int main()
{
    Attendee A,B;
    string input, output;
    cout << "please enter the input file name" << endl;// get the input file
    cin >> input;
    cout << "Please enter the output file name" << endl;// get the output file.
    cin >> output;
    ifstream fin(input);
    if (!(fin.good()))// if the file is not valid
    {
        cout << "invalid file name";
        system("pause");
        return 0;
    }
    ofstream fout(output);
    int size = 0;
    int arrused = 0;
    int balance = 0;
    int totalnumberofAt = 0;
    int totalfull = 0;
    int totalcontest = 0;
    Attendee* listOf = new Attendee[size];// We will use this to store our list of Attendees
    
    while (fin >> A)// While there is still information in the file
    {
        if (arrused == size)//if we need to resize the array
        {
            size = resizeArray(listOf, size);
        }
        B = A;
        listOf[arrused] = B;
        balance += listOf[arrused].Balance();// keep track of the total balance
        totalfull += listOf[arrused].getFullsize();// keep track of the total number of full students
        totalcontest += listOf[arrused].getcontestSize();// keep track of the total number of contest.
        arrused++;
    }
    totalnumberofAt = arrused;
    sortArray(listOf, size);

    for (int k = 0; k < size; k++)// this will output the information
    {
        fout << setw(30) << left << listOf[k].getName();
        fout << setw(40) << right << listOf[k].getinstitution() << endl;
        fout << setw(3) << right << listOf[k].getFullsize();
        fout << setw(25) << left << " Student full-Conference";
        fout << setw(3) << right << listOf[k].getcontestSize();
        fout << setw(25) << left << " Student Contest";
        fout << "$" << fixed << setprecision(2) << setw(10) << right << listOf[k].Balance() << endl;
        fout << endl;


    }
    fout << "Total number of Attendees: " << totalnumberofAt << endl;
    fout << "Total number of full-conference students " << totalfull << endl;
    fout << "total number of contest only students " << totalcontest << endl;
    fout << "Total Balance " << "$" << fixed << setprecision(2) << balance;
    system("pause");
    delete[] listOf;
}

