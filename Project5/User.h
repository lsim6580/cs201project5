
#ifndef USER_H
#define USER_H

#include <iostream>
#include <string> 
using namespace std; 

class User
{
public:
	User(); 
	~User(); 
	User(const User& other); 
	 
	void CheckOut(string item); 


private:
	string Name; 
	int ID; 

	// want dynamic array of strings to hold info
	// on checked-out items 
	string *CheckedOut; 
	int ArrSize, ArrUsed;
}; 
#endif 