#include <iostream>
#include <fstream>
#include <string>
#pragma once
using namespace std;

class Attendee
{

public:
    Attendee();// default constructor
    ~Attendee();// deconstructor
    Attendee(const Attendee& other);//copy constructor
    void setName(string setname);// accepts a string sets name to setname
    void setinstitution(string inst);// accepts a string sets institution to inst
    void setID(int number);// accepts int and sets ID to number
    void setinvoice(int number);//accepts in and sets invoice to number
    bool addfull(string name);// accepts name and adds to the full array
    bool removefull(string name);// accepts name and removes from the full array
    bool addcontest(string name);// accepts name and add to the contest array
    bool removecontest(string name);// accepts name and removes the name from contest array
    const double Balance();// returns the balance
    int getFullsize();// returns the number of students in the full conference
    int getcontestSize();// returns the number of students in the contest only
    string getName();// returns the name of the attendee
    string getinstitution();//returns the name of the institution
    int getID();// returns the ID number
    int getinvoice();// returns the invoice number
    const Attendee& operator= (const Attendee& rhs);
    friend ofstream& operator << (ofstream& out,Attendee& A);
    friend ifstream& operator >> (ifstream& in, Attendee& A);
private:
    string name;
    string institution;
    int ID;
    int invoice;
    int fullsize;
    int contestsize;
    int fullarrused;
    int contestarrused;
    string* fullAttendees;
    string* contestAttendees;

};